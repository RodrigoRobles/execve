## Execve

## Description
This is a example of how to call SYS_EXECVE from Linux x86_64 Assembly NASM.
This program uses SYS_EXECVE to invoke bash to execute the following command line:

    export DISPLAY=:0;/bin/xrandr | grep "*" | cut -b 4- | cut -d ' ' -f 1 > temp.txt

This line gets the resolution of the display and put into a file called temp.txt.
Explaining command by command:
- export DISPLAY=:0; Export this required environment variable
- /bin/xrandr ;executes xrandr
- grep "\*" ;select the line with the "*"
- cut -b 4- ;cut left part of the line
- cut -d ' ' -f 1 ;cut right part of the line 
- \> temp.txt" ; send result to file temp.txt

## License
Licensed under GPLV3;
