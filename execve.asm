;	execve.asm
;	Assembly Linux x86_64 SYS_EXECVE example
;	To assemble:
;		nasm -f elf64 execve.asm
;	To link:
;		ld -o execve execve.o
;	To run:
;		./execve
;-------------------------------------------------------------------------------------

section .data
  SYS_EXECVE equ 59
  file db `/bin/bash`, 0  
  ARGV0 db "bash", 0
  ARGV1 db "-c", 0
  ARGV2 db `export DISPLAY=:0;/bin/xrandr | grep \"*\" | cut -b 4- | cut -d \' \' -f 1 > temp.txt`, 0
  argv dq ARGV0, ARGV1, ARGV2, 0	

section .text
global _start

_start:

  mov rax, SYS_EXECVE
  mov rdi, file  
  mov rsi, argv
  xor rdx, rdx ;NULL as envp
  syscall

